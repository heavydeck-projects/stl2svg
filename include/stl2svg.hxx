#ifndef __STL2SVG_HXX
#define __STL2SVG_HXX

#include <string>
#include <algebra.hxx>

enum log_level{
    LOG_DISABLE = 0,
    LOG_ERROR,
    LOG_WARNING,
    LOG_INFO,
    LOG_DEBUG,
    LOG_TRACE,
};

struct config_s {
    config_s();
    virtual ~config_s();

    int logLevel;
    std::string inFile;
    std::string outFile;

    //Coordinate system for the projection plane.
    //Points will be projected against the z-plane of the given base
    float basis[3*3];

    //Normal vector filtering
    float normalFilter[3];
};

void log(const config_s &c, int log_level, const std::string &s);
int stl2svg(struct config_s &cfg);

// Classes used by stl2svg
struct Triangle{
    Triangle();
    virtual ~Triangle();
    std::string toString() const;

    float normal[3];
    float v1[3];
    float v2[3];
    float v3[3];
};
#endif
