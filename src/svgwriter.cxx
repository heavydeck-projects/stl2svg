/**
 * @file svgwriter.cxx
 * @brief Writes the transformed polygon data into an SVG document.
 */

/*
STL to SVG
Copyright (C) 2023  J.Luis Álvarez

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
*/

#include <svgwriter.hxx>
#include <stl2svg.hxx>
#include <float.h>
#include <sstream>
#include <iomanip>

void write_svg(const struct config_s &cfg, const std::vector<Triangle> &triangles, FILE* f)
{
    //Find the extreme points
    float x_max = FLT_MIN;
    float x_min = FLT_MAX;
    float y_max = FLT_MIN;
    float y_min = FLT_MAX;
    for (const Triangle &t : triangles){
        //X max
        x_max = (x_max > t.v1[0]) ? x_max : t.v1[0];
        x_max = (x_max > t.v2[0]) ? x_max : t.v2[0];
        x_max = (x_max > t.v3[0]) ? x_max : t.v3[0];
        //Y max
        y_max = (y_max > t.v1[1]) ? y_max : t.v1[1];
        y_max = (y_max > t.v2[1]) ? y_max : t.v2[1];
        y_max = (y_max > t.v3[1]) ? y_max : t.v3[1];

        //X min
        x_min = (x_min < t.v1[0]) ? x_min : t.v1[0];
        x_min = (x_min < t.v2[0]) ? x_min : t.v2[0];
        x_min = (x_min < t.v3[0]) ? x_min : t.v3[0];
        //Y min
        y_min = (y_min < t.v1[1]) ? y_min : t.v1[1];
        y_min = (y_min < t.v2[1]) ? y_min : t.v2[1];
        y_min = (y_min < t.v3[1]) ? y_min : t.v3[1];
    }
    //Log the extreme points
    {
        std::stringstream ss;
        ss << std::fixed << std::setprecision(3);
        ss << "Extreme points: " 
            << "(" << x_min << ", " << y_min << ") "
            << "(" << x_max << ", " << y_max << ")";
        ss << std::flush;
        log(cfg, LOG_DEBUG, ss.str());
    }

    //Write the SVG document header
    {
        static const char s[] =
        "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>\n"
        "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 20001102//EN\"\n"
        "\t\"http://www.w3.org/TR/2000/CR-SVG-20001102/DTD/svg-20001102.dtd\">\n"
        ;
        int wb = fwrite(s, strnlen(s, sizeof(s)), 1, f);
    }

    //Write the SVG tag
    {
        float width = abs(x_max - x_min);
        float height = abs(y_max - y_min);
        std::stringstream ss;
        ss << "<svg width=\"" << width <<"\" height=\"" << height << "\">\n";
        ss << "<!-- Generated with stl2svg -->\n" << std::flush;;
        int wb = fwrite(ss.str().c_str(), ss.str().length(), 1, f);
    }

    //Write the <g> tag with a transform moving all the poligons _inside_ the document.
    {
        std::stringstream ss;
        ss << "<g transform=\"" << "translate(" << (-x_min) << " " << (-y_min) << ")" << "\">\n";
        int wb = fwrite(ss.str().c_str(), ss.str().length(), 1, f);
    }

    //Write all the polygons
    for (const Triangle &t : triangles){
        std::stringstream ss;
        ss << "\t<polygon points=\"";
        ss << t.v1[0] << "," << t.v1[1] << " ";
        ss << t.v2[0] << "," << t.v2[1] << " ";
        ss << t.v3[0] << "," << t.v3[1];
        ss << "\" />\n" << std::flush;
        int wb = fwrite(ss.str().c_str(), ss.str().length(), 1, f);
    }

    //close the tags
    {
        static const char s[] =
        "</g>\n"
        "</svg>\n"
        ;
        int wb = fwrite(s, strnlen(s, sizeof(s)), 1, f);
    }
    
    log(cfg, LOG_TRACE, "--- BEGIN SVG writting ---");

    log(cfg, LOG_TRACE, "--- END SVG writting ---");
}