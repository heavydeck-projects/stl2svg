#ifndef __ALGEBRA_HXX
#define __ALGEBRA_HXX

/**
 * @file algebra.hxx
 * @brief Implements algebraic operations over 3D/2D vectors & matrices.
 */

/*
STL to SVG
Copyright (C) 2023  J.Luis Álvarez

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
*/

#include <math.h>
#include <string.h>

//Some helper algebra oprations.
//All functions should be safe to use with `src` and `dst` being the same pointer.

// ------------------
// --- 2D algebra ---
// ------------------

static inline float v2_length(const float *v){
    return sqrt(
        (v[0] * v[0]) +
        (v[1] * v[1])
    );
}

static inline void v2_normalize(const float* src, float* dst){
    const float len = v2_length(src);
    dst[0] = src[0] / len;
    dst[1] = src[1] / len;
}

// ------------------
// --- 3D algebra ---
// ------------------

static inline bool v3_equality(const float* v1, const float* v2, float epsilon=0.001){
    bool rv = true;
    rv = rv && (abs(v1[0] - v2[0]) < epsilon);
    rv = rv && (abs(v1[1] - v2[1]) < epsilon);
    rv = rv && (abs(v1[2] - v2[2]) < epsilon);
    return rv;
}

static inline float v3_length(const float *v){
    return sqrt(
        (v[0] * v[0]) +
        (v[1] * v[1]) +
        (v[2] * v[2])
    );
}

static inline void v3_normalize(const float* src, float* dst){
    const float len = v3_length(src);
    dst[0] = src[0] / len;
    dst[1] = src[1] / len;
    dst[2] = src[2] / len;
}

static inline float v3_dot_product(const float* v1, const float* v2){
    return (v1[0]*v2[0]) + (v1[1]*v2[1]) + (v1[2]*v2[2]);
}

//Traspose a 3x3 matrix
static inline void m3x3_traspose(const float* src, float* dst){
    float rv[3*3];

    rv[(0*3) + 0] = src[(0*3) + 0]; rv[(0*3) + 1] = src[(1*3) + 0]; rv[(0*3) + 2] = src[(2*3) + 0];
    rv[(1*3) + 0] = src[(0*3) + 1]; rv[(1*3) + 1] = src[(1*3) + 1]; rv[(1*3) + 2] = src[(2*3) + 1];
    rv[(2*3) + 0] = src[(0*3) + 2]; rv[(2*3) + 1] = src[(1*3) + 2]; rv[(2*3) + 2] = src[(2*3) + 2];

    memcpy(dst, rv, sizeof(float) * (3*3));
}

//Vector multiplication with a 3x3 matrix
static inline void v3_mult_m3x3(const float* v, const float* matx, float* dst){
    float rv[3]; //Temporary storage of the result

    //Element access: matx[ (row * 3) + c ]
    rv[0] = (matx[ (0 * 3) + 0 ] * v[0]) + (matx[ (1 * 3) + 0 ] * v[1]) + (matx[ (2 * 3) + 0 ] * v[2]);
    rv[1] = (matx[ (0 * 3) + 1 ] * v[0]) + (matx[ (1 * 3) + 1 ] * v[1]) + (matx[ (2 * 3) + 1 ] * v[2]);
    rv[2] = (matx[ (0 * 3) + 2 ] * v[0]) + (matx[ (1 * 3) + 2 ] * v[1]) + (matx[ (2 * 3) + 2 ] * v[2]);

    memcpy(dst, rv, sizeof(float) * 3);
}

//3x3 matrix multiplication with vector
static inline void m3x3_mult_v3(const float* matx, const float* v, float* dst){
    float rv[3]; //Temporary storage of the result

    //Element access: matx[ (row * 3) + c ]
    rv[0] = (matx[ (0 * 3) + 0 ] * v[0]) + (matx[ (0 * 3) + 1 ] * v[1]) + (matx[ (0 * 3) + 2 ] * v[2]);
    rv[1] = (matx[ (1 * 3) + 0 ] * v[0]) + (matx[ (1 * 3) + 1 ] * v[1]) + (matx[ (1 * 3) + 2 ] * v[2]);
    rv[2] = (matx[ (2 * 3) + 0 ] * v[0]) + (matx[ (2 * 3) + 1 ] * v[1]) + (matx[ (2 * 3) + 2 ] * v[2]);

    //Copy result to dst
    memcpy(dst, rv, sizeof(float) * 3);
}

//3x3 matrix multiplication with a scalar
static inline void m3x3_mult_scalar(const float* src, float s, float* dst){
    float rv[3*3];
    for(int i = 0; i < 3*3; i++){
        rv[i] = src[i] * s;
    }
    memcpy(dst, rv, sizeof(float)*(3*3));
}

static inline float m3x3_determinant(const float* m){
    float rv =
        (
            (m[(0 *3) + 0] * m[(1 *3) + 1] * m[(2 *3) + 2]) + 
            (m[(0 *3) + 1] * m[(1 *3) + 2] * m[(2 *3) + 0]) + 
            (m[(0 *3) + 2] * m[(1 *3) + 0] * m[(2 *3) + 1])
        )
        -
        (
            (m[(0 *3) + 2] * m[(1 *3) + 1] * m[(2 *3) + 0]) + 
            (m[(0 *3) + 1] * m[(1 *3) + 0] * m[(2 *3) + 2]) + 
            (m[(0 *3) + 0] * m[(1 *3) + 2] * m[(2 *3) + 1])
        )
    ;
    return rv;
}

//Invert a 3x3 matrix.
static inline void m3x3_invert(const float* src, float* dst){
    float rv[3*3];

    //Create the adjoints matrix
    rv[(0 *3) + 0] =  ((src[(1 *3) + 1] * src[(2 *3) + 2]) - (src[(1 *3) + 2] * src[(2 *3) + 1]));
    rv[(0 *3) + 1] = -((src[(1 *3) + 0] * src[(2 *3) + 2]) - (src[(1 *3) + 2] * src[(2 *3) + 0]));
    rv[(0 *3) + 2] =  ((src[(1 *3) + 0] * src[(2 *3) + 1]) - (src[(1 *3) + 1] * src[(2 *3) + 0]));

    rv[(1 *3) + 0] = -((src[(0 *3) + 1] * src[(2 *3) + 2]) - (src[(0 *3) + 2] * src[(2 *3) + 1]));
    rv[(1 *3) + 1] =  ((src[(0 *3) + 0] * src[(2 *3) + 2]) - (src[(0 *3) + 2] * src[(2 *3) + 0]));
    rv[(1 *3) + 2] = -((src[(0 *3) + 0] * src[(2 *3) + 1]) - (src[(0 *3) + 1] * src[(2 *3) + 0]));

    rv[(2 *3) + 0] =  ((src[(0 *3) + 1] * src[(1 *3) + 2]) - (src[(0 *3) + 2] * src[(1 *3) + 1]));
    rv[(2 *3) + 1] = -((src[(0 *3) + 0] * src[(1 *3) + 2]) - (src[(0 *3) + 2] * src[(1 *3) + 0]));
    rv[(2 *3) + 2] =  ((src[(0 *3) + 0] * src[(1 *3) + 1]) - (src[(0 *3) + 1] * src[(1 *3) + 0]));

    //Traspose it
    m3x3_traspose(rv,rv);

    //Divide by its determinant
    float det = m3x3_determinant(src);
    m3x3_mult_scalar(rv, 1.0/det, rv);

    memcpy(dst, rv, sizeof(float) * (3*3));
}

#endif
