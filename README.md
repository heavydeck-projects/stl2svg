# STL2SVG

A tool for converting an STL model into SVG by projecting its poligons
against a given plane.

You may check the basic program usage by running `stl2svg --help`

## Building

Building needs:

* CMake
* A C++ compiler
* Subrepository extensions for git (optional)

After cloning the repository, fetch the optparse dependency, included as a
subrepository on this project

```sh
git subrepo fetch --all
```

Or alternatively, copy `optparse.h` on the `optparse/` directory, which you
can get from the [optparse project repository][optparse]. Once this
is completed, configure and build the project like any other CMake project.
For example like this:

[optparse]: https://github.com/skeeto/optparse

```sh
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release .
make
```

## License

STL to SVG
Copyright (C) 2023  J.Luis Álvarez

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
