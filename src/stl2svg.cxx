/**
 * @file stl2svg.cxx
 * @brief Implements STL parsing and preparation of polygon data.
 */

/*
STL to SVG
Copyright (C) 2023  J.Luis Álvarez

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
*/

#include <stl2svg.hxx>
#include <algebra.hxx>
#include <svgwriter.hxx>

#include <iostream>
#include <iomanip>
#include <memory>
#include <sstream>
#include <vector>
#include <algorithm>

#include <cmath>
#include <cstdio>
#include <cstdint>
#include <cstring>

//Maximum acceptable deviation from exact unitary value
#define EPSILON 0.001
#define MOSTLY_EQUAL(X,Y) (std::abs((X) - (Y)) < (EPSILON))
#define MOSTLY_UNITARY(X) MOSTLY_EQUAL(1.0, X)
// --- Configuration defaults & destructor ---
config_s::config_s(){
    logLevel = LOG_WARNING;

    basis[(0*3) + 0] = 1.0; basis[(0*3) + 1] = 0.0; basis[(0*3) + 2] = 0.0;
    basis[(1*3) + 0] = 0.0; basis[(1*3) + 1] = 1.0; basis[(1*3) + 2] = 0.0;
    basis[(2*3) + 0] = 0.0; basis[(2*3) + 1] = 0.0; basis[(2*3) + 2] = 1.0;

    normalFilter[0] = 0.0; normalFilter[1] = 0.0; normalFilter[2] = 0.0;
}

config_s::~ config_s(){
    //Empty
}
// -------------------------------------------

// --- class Triangle
Triangle::Triangle(){

}
Triangle::~Triangle(){

}
std::string Triangle::toString() const{
    std::stringstream ss;
    ss << std::fixed << std::setprecision(3);
    ss << "Triangle:" << std::endl;
    ss << "    NV: " << this->normal[0] << ", " << this->normal[1] << ", " << this->normal[2] << std::endl;
    ss << "    V1: " << this->v1[0] << ", " << this->v1[1] << ", " << this->v1[2] << std::endl;
    ss << "    V2: " << this->v2[0] << ", " << this->v2[1] << ", " << this->v2[2] << std::endl;
    ss << "    V3: " << this->v3[0] << ", " << this->v3[1] << ", " << this->v3[2] << std::flush;

    return ss.str();
}
// --- Triangle related non-member functions
bool z_less_than(const Triangle& l, const Triangle& r){
    float lz = std::min(std::min(l.v1[2], l.v2[2]), l.v3[2]);
    float rz = std::min(std::min(r.v1[2], r.v2[2]), r.v3[2]);
    return lz < rz;
}

void log(const config_s &cfg, int log_level, const std::string &s){
    if(log_level > cfg.logLevel){
        return;
    }
    std::cerr << s << std::endl;
}

int stl2svg(config_s &cfg)
{
    //As long as thi isn't false, continue doing your thing
    bool all_ok = true;

    // --- Initialization

    //Open input and output files
    //Use stdin/out by default
    std::FILE* file_in = stdin;
    if(cfg.inFile.length()){
        file_in = fopen(cfg.inFile.c_str(), "rb");
    }
    if(file_in == nullptr){
        log(cfg, LOG_ERROR, "Unable to open input file: " + cfg.inFile);
        all_ok = false;
    }

    std::FILE* file_out = stdout;
    if(cfg.outFile.length()){
        file_out = fopen(cfg.outFile.c_str(), "wb");
    }
    if(file_out == nullptr){
        log(cfg, LOG_ERROR, "Unable to open output file: " + cfg.inFile);
        all_ok = false;
    }
    
    //Some logs from configuration
    if(all_ok){
        //Files used
        log(cfg, LOG_DEBUG, "Reading from: " + ((file_in == stdin) ? "<stdin>" : cfg.inFile));
        log(cfg, LOG_DEBUG, "Writing to: " + ((file_out == stdout) ? "<stdout>" : cfg.outFile));
        //Output base
        {
            std::stringstream ss;
            ss << std::fixed << std::setprecision(3);
            ss << "Output base:" << std::endl;
            ss << "    x: " << cfg.basis[(0*3) + 0] << ", " << cfg.basis[(0*3) + 1] << ", " << cfg.basis[(0*3) + 2]<< std::endl;
            ss << "    y: " << cfg.basis[(1*3) + 0] << ", " << cfg.basis[(1*3) + 1] << ", " << cfg.basis[(1*3) + 2]<< std::endl;
            ss << "    z: " << cfg.basis[(2*3) + 0] << ", " << cfg.basis[(2*3) + 1] << ", " << cfg.basis[(2*3) + 2]<< std::flush;
            log(cfg, LOG_DEBUG, ss.str());
        }
    }

    //Normalize the filter vector if not zero and not unitary already.
    if(all_ok){
        if(
            (cfg.normalFilter[0] != 0.0) &&
            (cfg.normalFilter[1] != 0.0) &&
            (cfg.normalFilter[2] != 0.0) &&
            (!MOSTLY_UNITARY(v3_length(cfg.normalFilter)))
        )
        {
            v3_normalize(cfg.normalFilter, cfg.normalFilter);
        }
    }

    // --- Parse STL file

    //Verify STL header *IS NOT* ASCII
    if(all_ok){
        uint8_t header[80];
        size_t header_size = fread(header, 1, 80, file_in);
        if(header_size != 80){
            log(cfg, LOG_ERROR, "Could nor read STL header");
            all_ok = false;
        }
        else{
            const char* solid = "solid";
            if (memcmp(header, solid, std::strlen(solid)) == 0){
                log(cfg, LOG_ERROR, "Detected ASCII STL format; Exitting.");
                all_ok = false;
            }
        }
        //Dump header (trace)
        {
            std::stringstream ss;
            ss << "Header: ";
            //char
            for(int i = 0; i<sizeof(header); i++){
                if((header[i] <= 126) && (header[i] >= 32)){
                    ss << (char) header[i];
                }
                else{
                    ss << ".";
                }
            }
            ss << std::flush;
            log(cfg, LOG_DEBUG, ss.str());
        }
    }

    //Get triangle count
    uint32_t triangle_count = 0;
    if(all_ok){
        size_t rb = fread(&triangle_count, sizeof(uint32_t), 1, file_in);
        if(rb != 1){
            log(cfg, LOG_ERROR, "Unable to read triangle count.");
            all_ok = false;
        }
        else{
            log(cfg, LOG_INFO, "STL triangle count: " + std::to_string(triangle_count));
        }
    }

    //parse every triangle & save them to a vector
    log(cfg, LOG_TRACE, "--- BEGIN STL triangle parsing ---");
    std::vector<Triangle> triangles;
    triangles.reserve(triangle_count);
    while(all_ok && triangle_count){
        //auto t = std::make_shared<Triangle>();
        Triangle t;
        //Read the 4 vectors
        size_t rc = 0;
        rc += fread(t.normal, sizeof(float), 3, file_in);
        rc += fread(t.v1, sizeof(float), 3, file_in);
        rc += fread(t.v2, sizeof(float), 3, file_in);
        rc += fread(t.v3, sizeof(float), 3, file_in);
        //Read count must be 12
        if(rc != 12){
            log(
                cfg, LOG_ERROR,
                "Unable to read triangle data (" + std::to_string(triangle_count) + " triangles remain unread)"
            );
            all_ok = false;
            break;
        }
        triangles.push_back(t);

        //Skip attributes
        uint16_t attribute_bytes = 0;
        fread(&attribute_bytes, sizeof(uint16_t), 1, file_in);
        while(attribute_bytes){
            uint8_t b;
            fread(&b, sizeof(uint8_t), 1, file_in);
        }

        //Trace-print
        log(cfg, LOG_TRACE, t.toString());

        //Next!
        triangle_count--;
    }
    log(cfg, LOG_TRACE, "--- END STL triangle parsing ---");
    log(cfg, LOG_DEBUG, "Triangles parsed: " + std::to_string(triangles.size()));

    // --- Process triangle data

    //Perform a base change on all triangles
    if(all_ok){
        log(cfg, LOG_TRACE, "--- BEGIN triangle basis change ---");
        
        //Calculate the basis-change matrix using cfg.basis
        float basis_change_matrix[3*3];
        // - Traspose the basis matrix
        m3x3_traspose(cfg.basis, basis_change_matrix);
        // - Get its determinant
        float bcm_determinant = m3x3_determinant(basis_change_matrix);
        if(MOSTLY_EQUAL(0.0, bcm_determinant)){
            log(cfg, LOG_WARNING, "Base change matrix might be degenerate, erroneous results may be produced");
        }
        // - Invert it
        m3x3_invert(basis_change_matrix, basis_change_matrix);

        //For each triangle, apply the transformation to its components
        for(auto &t : triangles){
            m3x3_mult_v3(basis_change_matrix, t.normal, t.normal);
            m3x3_mult_v3(basis_change_matrix, t.v1, t.v1);
            m3x3_mult_v3(basis_change_matrix, t.v2, t.v2);
            m3x3_mult_v3(basis_change_matrix, t.v3, t.v3);
            log(cfg, LOG_TRACE, t.toString());
        }
        log(cfg, LOG_TRACE, "--- END triangle basis change ---");
    }

    //Cull all triangles more than 90º away if a normal vector is provided
    if(all_ok && MOSTLY_UNITARY(v3_length(cfg.normalFilter))){
        log(cfg, LOG_TRACE, "--- BEGIN triangle culling ---");
        std::vector<Triangle> new_triangles;
        for(auto t : triangles){
            //Calculate cosine of filter & normal vectors
            float normalized_normal[3];
            v3_normalize(t.normal, normalized_normal);
            float v_cosine = v3_dot_product(cfg.normalFilter, normalized_normal);
            //We *do* want to filter out polygons that are dead-on 90º so the
            //actual angle we are filtering against is arccos(0.001) => 89.94º
            if(v_cosine > 0.001){
                new_triangles.push_back(t);
                log(cfg, LOG_TRACE, t.toString());
            }
        }
        triangles = new_triangles;
        log(cfg, LOG_TRACE, "--- END triangle culling ---");
    }

    //Sort triangles by increasing Z axis.
    //Don't really care about overlaps
    if(all_ok){
        std::sort(triangles.begin(), triangles.end(), z_less_than);
    }

    //Send the processed triangle collection to SVG writer
    if(all_ok){
        write_svg(cfg, triangles, file_out);
    }

    // --- Cleanup & exit ---

    //Free file handles
    {
        if(file_in  && (file_in != stdin))  fclose(file_in);
        if(file_out && (file_out != stdin)) fclose(file_out);
    }

    //Return success if all ok
    if(all_ok){
        return 0;
    }
    else{
        return 1;
    }
}
