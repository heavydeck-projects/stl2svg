#ifndef __SVG_WRITER_HXX
#define __SVG_WRITER_HXX

#include <vector>
#include <stdio.h>

class Triangle;
struct config_s;
void write_svg(const struct config_s &cfg, const std::vector<Triangle> &triangles, FILE* f);

#endif
