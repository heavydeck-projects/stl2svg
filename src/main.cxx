/**
 * @file main.cxx
 * @brief Main file; Entry point and argument parsing.
 */

/*
STL to SVG
Copyright (C) 2023  J.Luis Álvarez

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
*/

#include <stl2svg.hxx>

#define OPTPARSE_IMPLEMENTATION
#include <optparse.h>

#include <iostream>
#include <iomanip>
#include <sstream>
#include <cstdint>

static void print_version(){
    std::cout << "VERSION" << std::endl;
}

static const std::string help_str[] = {
    "stl2svg [OPTIONS] [-n[<VECTOR>]] [-o<OUTPUT FILE>] [<INPUT FILE>]",
    "",
    "<INPUT FILE>",
    "  If given, STL data will be read from <INPUT FILE> instead of stdin",
    "-o<FILE>  --output=<FILE>",
    "  Writes SVG output to <FILE> instead of stdout",
    //-- I am 80 characters wide ------------------------------------------------//
    "-x -y -z",
    "  Project triangles against the x/y/z plane; By default the z plane is used",
    "-x<VECTOR> -y<VECTOR> -z<VECTOR>",
    "  Define the x/y/z vectors of an algebraic base, the z-plane of which will be",
    "  used to project all the points against it. Vector format should be",
    "  X:Y:Z where X/Y/Z are decimal numbers; For example: -x 1.0:0.0:0.0",
    "-n[<VECTOR>]  --normal-filter[=<VECTOR>]",
    "  Output only polygons up to 90º away from the given vector. If no vector",
    "  given, +z is used (0.0:0.0:1.0); By default no filtering is performed",
    "-v  --verbose",
    "  Increase program verbosity with -v; May be used multiple times",
    "-q  --quiet",
    "  Disable all status outputs",
    "-V  --version",
    "  Prints program version",
    "-h  --help",
    "  Prints this message",
};

static void print_help(){
    for(const std::string &s : help_str){
        std::cout << s << std::endl;
    }
}

static void parse_vector3f(const char* s, float* out_v){
    std::istringstream ss(s);
    std::string token;
    int i = 0;
    while( (std::getline(ss, token, ':')) && (i < 3)){
        out_v[i] = std::stof(token);
        i++;
    }
}

int main(int argc, char** argv){
    //Make default config
    config_s cfg;

    //Parse command line and modify cfg accordingly
    {
        struct optparse_long longopts[] = {
            {"help",    'h', OPTPARSE_NONE},
            {"version", 'V', OPTPARSE_NONE},

            {"verbose", 'v', OPTPARSE_NONE},
            {"quiet",   'q', OPTPARSE_NONE},
            {"output",  'o', OPTPARSE_REQUIRED},

            //Output coordinate system
            {"project-x",  'x', OPTPARSE_OPTIONAL},
            {"project-y",  'y', OPTPARSE_OPTIONAL},
            {"project-z",  'z', OPTPARSE_OPTIONAL},

            //Filtering
            {"normal-filter", 'n', OPTPARSE_OPTIONAL},

            {nullptr,   '\0', OPTPARSE_NONE}
        };
        struct optparse options;
        optparse_init(&options, argv);

        //Parse named arguments
        int option;
        while ((option = optparse_long(&options, longopts, nullptr)) != -1) {
            switch (option)
            {
            case 'h':
            //Show help and exit
            print_help();
            return 0;

            case 'V':
            //Show version and exit
            print_version();
            return 0;

            case 'v':
            cfg.logLevel++;
            break;

            case 'q':
            cfg.logLevel = LOG_DISABLE;
            break;

            case 'o':
            if(cfg.outFile.length()){
                log(
                    cfg, LOG_WARNING,
                    "Multiple output files; Using last one: " + std::string(options.optarg)
                );
            }
            cfg.outFile = std::string(options.optarg);
            break;

            case 'x':
            if(!options.optarg){
                cfg.basis[(0*3) + 0] = 0.0; cfg.basis[(0*3) + 1] = 0.0; cfg.basis[(0*3) + 2] = 1.0;
                cfg.basis[(1*3) + 0] = 0.0; cfg.basis[(1*3) + 1] = 1.0; cfg.basis[(1*3) + 2] = 0.0;
                cfg.basis[(2*3) + 0] = 1.0; cfg.basis[(2*3) + 1] = 0.0; cfg.basis[(2*3) + 2] = 0.0;
            }
            else{
                parse_vector3f(options.optarg, cfg.basis + (0*3));
            }
            break;

            case 'y':
            if(!options.optarg){
                cfg.basis[(0*3) + 0] = 1.0; cfg.basis[(0*3) + 1] = 0.0; cfg.basis[(0*3) + 2] = 0.0;
                cfg.basis[(1*3) + 0] = 0.0; cfg.basis[(1*3) + 1] = 0.0; cfg.basis[(1*3) + 2] = 1.0;
                cfg.basis[(2*3) + 0] = 0.0; cfg.basis[(2*3) + 1] = 1.0; cfg.basis[(2*3) + 2] = 0.0;
            }
            else{
                parse_vector3f(options.optarg, cfg.basis + (1*3));
            }
            break;

            case 'z':
            if(!options.optarg){
                cfg.basis[(0*3) + 0] = 1.0; cfg.basis[(0*3) + 1] = 0.0; cfg.basis[(0*3) + 2] = 0.0;
                cfg.basis[(1*3) + 0] = 0.0; cfg.basis[(1*3) + 1] = 1.0; cfg.basis[(1*3) + 2] = 0.0;
                cfg.basis[(2*3) + 0] = 0.0; cfg.basis[(2*3) + 1] = 0.0; cfg.basis[(2*3) + 2] = 1.0;
            }
            else{
                parse_vector3f(options.optarg, cfg.basis + (2*3));
            }
            break;

            case 'n':
            if(!options.optarg){
                cfg.normalFilter[0] = 0.0f; cfg.normalFilter[1] = 0.0f; cfg.normalFilter[2] = 1.0f;
            }
            else{
                parse_vector3f(options.optarg, cfg.normalFilter);
            }
            break;
            
            case '?':
            //Unknown argument
            log(
                cfg, LOG_ERROR,
                "Error: " + std::string(options.errmsg)
            );
            return 1;
            }
        }
        //Parse remaining (unnamed) arguments
        char* arg;
        while ((arg = optparse_arg(&options))){
            if(cfg.inFile.length()){
                log(
                    cfg, LOG_WARNING,
                    "Multiple input files; Using last one: " + std::string(arg)
                );
            }
            cfg.inFile = std::string(arg);
        }
    }

    //Endianness & datatypes check
    {
        //Endianness; Make sure the cow is not dead
        uint32_t schroedinger_cow = 0xDEADBEEFu;
        uint16_t *cow = (uint16_t*) & schroedinger_cow;
        if(*cow == 0xBEEFu){
            //Endian OK
            log(cfg, LOG_TRACE, "Endian check OK");
        }
        else{
            //Bad endian
            log(
                cfg, LOG_ERROR,
                "Endian check Failed; This program only works on little-endian systems"
            );
            return 1;
        }

        //float == 4bytes
        if(sizeof(float) != 4){
            log(
                cfg, LOG_ERROR,
                "Float is not 4 bytes long; Exitting"
            );
            return 1;
        }
    }

    //Return the real program main using the config struct
    return stl2svg(cfg);
}
